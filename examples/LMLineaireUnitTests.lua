require "LeroyMerlin"

local baseGap = 15

-------------------------------------------------------------------------------

local thinShelfBody = Shelf:new()
thinShelfBody:setDirections({ Stack.DIRECTION.TOP, Stack.DIRECTION.LEFT })
thinShelfBody:setAlignments({ Stack.ALIGNMENT.BOTTOM, Stack.ALIGNMENT.BACK })

thinShelfBody:stack(Equipment_LM.TroisTiroirs:equip(Caisson_LM["B"]["S"]["Sa"]:new()))
thinShelfBody:stack(Equipment_LM.UnTiroirUnePorteOpeningRight:equip(Caisson_LM["B"]["S"]["Sa"]:new()))
thinShelfBody:nextStack()
thinShelfBody:stack(TransparentVolume:new{ height = 60 })
thinShelfBody:nextStack()
thinShelfBody:stack(Equipment_LM.TwoDoors:equip(Caisson_LM["H"]["M"]["M"]["L"]:new()))

-- print(thinShelfBody:describe())

-------------------------------------------------------------------------------

local wideShelfBody = Shelf:new()
wideShelfBody:setDirections({ Stack.DIRECTION.TOP, Stack.DIRECTION.LEFT })
wideShelfBody:setAlignments({ Stack.ALIGNMENT.BOTTOM, Stack.ALIGNMENT.BACK, Stack.ALIGNMENT.RIGHT })

wideShelfBody:stack(Equipment_LM.TroisTiroirsEgaux:equip(Caisson_LM["B"]["M"]["M"]:new()))
wideShelfBody:stack(Equipment_LM.DeuxTiroirsUnePorteOpeningRight:equip(Caisson_LM["B"]["M"]["Sb"]:new()))
wideShelfBody:nextStack()
wideShelfBody:stack(TransparentVolume:new{ height = 60 })
wideShelfBody:nextStack()
wideShelfBody:stack(Equipment_LM.TwoDoors:equip(Caisson_LM["H"]["M"]["M"]["L"]:new()))

-- print("\n\n" .. wideShelfBody:describe())

-------------------------------------------------------------------------------

local completeShelfBody = HorizontalStack:new()
completeShelfBody:setDirection(Stack.DIRECTION.LEFT)
completeShelfBody:setAlignments({ Volume.SIDE.FRONT, Volume.SIDE.BOTTOM })
completeShelfBody:stack(thinShelfBody)
completeShelfBody:stack(Caisson_LM["C"]["M"]["M"]["L"]:new())
completeShelfBody:stack(wideShelfBody)

-------------------------------------------------------------------------------

local completeShelf = Shelf:new()
completeShelf:setDirections({ Stack.DIRECTION.TOP, Stack.DIRECTION.BACKWARD })
completeShelf:setAlignments({ Volume.SIDE.FRONT, Volume.SIDE.BOTTOM })

completeShelf:stack(TransparentVolume:new{ depth = baseGap })
completeShelf:stack(Plinthe:new{ width = completeShelfBody.width })
completeShelf:nextStack()
completeShelf:stack(completeShelfBody)

-- print("\n\n" .. completeShelf:describe())

-------------------------------------------------------------------------------

return completeShelf
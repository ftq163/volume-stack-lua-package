package.path = package.path .. ";../?/init.lua"

require "volumeStack"

local ut = require "LMLineaireUnitTests"

print("\n" .. ut:describe() .. "\n")

local utNode, utCode =
    ut:getTikzNode(
        Volume.SIDE.TOP, "south east", "(0,0)",
        { rotationAroundHeightAxis = 90 }
    )
print(string.format("\nutNode = '%s'", utNode))
print(string.format("utCode = '%s'", utCode))

-- [[

local utNode, utCode = ut:getTikzNode(Volume.SIDE.FRONT, "south east", "(0,0)")
print(string.format("\nutNode = '%s'", utNode))
print(string.format("utCode = '%s'", utCode))

local utNode, utCode = ut:getTikzNode(Volume.SIDE.LEFT, "south east", "(0,0)")
print(string.format("\nutNode = '%s'", utNode))
print(string.format("utCode = '%s'", utCode))

--]]

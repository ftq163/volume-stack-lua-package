require "Caissons"

--------------------------------------------------------------------------------

local C = {}

local Equipment = Class:new()

function Equipment:equip(volume)
    local equippedVolume = {}
    for k, v in pairs(self) do
        equippedVolume[k] = v
    end
    setmetatable(equippedVolume, volume)
    volume.__index = volume
    return equippedVolume
end

-- Equipement 3 tiroirs assymétriques
local TroisTiroirs = Equipment:new()
function TroisTiroirs:getTikzNode(seenFrom, anchor, at, extraParams)
    if seenFrom == Volume.SIDE.FRONT then
        local nodeName, nodeCode = getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
        local equippedNodeCode =
            string.format("%s\
  \\path ($(%s.north)!0!(%s.south)$) ++(0,-5\\unit) ++(-10\\unit,0) edge[handle] ++(+20\\unit,0);\
  \\path[cut] ($(%s.north west)!.16!(%s.south west)$) -- ($(%s.north east)!.16!(%s.south east)$);\
  \\path ($(%s.north)!.16!(%s.south)$) ++(0,-5\\unit) ++(-10\\unit,0) edge[handle] ++(+20\\unit,0);\
  \\path[cut] ($(%s.north west)!.5!(%s.south west)$) -- ($(%s.north east)!.5!(%s.south east)$);\
  \\path ($(%s.north)!.5!(%s.south)$) ++(0,-5\\unit) ++(-10\\unit,0) edge[handle] ++(+20\\unit,0);",
            nodeCode,
            nodeName, nodeName,
            nodeName, nodeName, nodeName, nodeName,
            nodeName, nodeName,
            nodeName, nodeName, nodeName, nodeName,
            nodeName, nodeName)
        return nodeName, equippedNodeCode
    else
        return getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
    end
end
C.TroisTiroirs = TroisTiroirs

-- Equipement 3 tiroirs égaux
local TroisTiroirsEgaux = Equipment:new()
function TroisTiroirsEgaux:getTikzNode(seenFrom, anchor, at, extraParams)
    if seenFrom == Volume.SIDE.FRONT then
        local nodeName, nodeCode = getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
        local equippedNodeCode =
            string.format("%s\
  \\path ($(%s.north)!0!(%s.south)$) ++(0,-5\\unit) ++(-10\\unit,0) edge[handle] ++(+20\\unit,0);\
  \\path[cut] ($(%s.north west)!.33!(%s.south west)$) -- ($(%s.north east)!.33!(%s.south east)$);\
  \\path ($(%s.north)!.33!(%s.south)$) ++(0,-5\\unit) ++(-10\\unit,0) edge[handle] ++(+20\\unit,0);\
  \\path[cut] ($(%s.north west)!.66!(%s.south west)$) -- ($(%s.north east)!.66!(%s.south east)$);\
  \\path ($(%s.north)!.66!(%s.south)$) ++(0,-5\\unit) ++(-10\\unit,0) edge[handle] ++(+20\\unit,0);",
            nodeCode,
            nodeName, nodeName,
            nodeName, nodeName, nodeName, nodeName,
            nodeName, nodeName,
            nodeName, nodeName, nodeName, nodeName,
            nodeName, nodeName)
        return nodeName, equippedNodeCode
    else
        return getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
    end
end
C.TroisTiroirsEgaux = TroisTiroirsEgaux

-- Equipement 2 tiroirs égaux
local DeuxTiroirs = Equipment:new()
function DeuxTiroirs:getTikzNode(seenFrom, anchor, at, extraParams)
    if seenFrom == Volume.SIDE.FRONT then
        local nodeName, nodeCode = getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
        local equippedNodeCode =
            string.format("%s\
  \\path ($(%s.north)!0!(%s.south)$) ++(0,-5\\unit) ++(-10\\unit,0) edge[handle] ++(+20\\unit,0);\
  \\path[cut] ($(%s.north west)!.5!(%s.south west)$) -- ($(%s.north east)!.5!(%s.south east)$);\
  \\path ($(%s.north)!.5!(%s.south)$) ++(0,-5\\unit) ++(-10\\unit,0) edge[handle] ++(+20\\unit,0);",
            nodeCode,
            nodeName, nodeName,
            nodeName, nodeName, nodeName, nodeName,
            nodeName, nodeName)
        return nodeName, equippedNodeCode
    else
        return getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
    end
end
C.DeuxTiroirs = DeuxTiroirs

-- Equipement 2 tiroirs et 1 porte
local DeuxTiroirsUnePorteOpeningRight = Equipment:new()
function DeuxTiroirsUnePorteOpeningRight:getTikzNode(seenFrom, anchor, at, extraParams)
    if seenFrom == Volume.SIDE.FRONT then
        local nodeName, nodeCode = getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
        local equippedNodeCode =
            string.format("%s\
  \\path ($(%s.north)!0!(%s.south)$) ++(0,-5\\unit) ++(-10\\unit,0) edge[handle] ++(+20\\unit,0);\
  \\path[cut] ($(%s.north west)!.16!(%s.south west)$) -- ($(%s.north east)!.16!(%s.south east)$);\
  \\path ($(%s.north)!.16!(%s.south)$) ++(0,-5\\unit) ++(-10\\unit,0) edge[handle] ++(+20\\unit,0);\
  \\path[cut] ($(%s.north west)!.33!(%s.south west)$) -- ($(%s.north east)!.33!(%s.south east)$);\
  \\path ($(%s.north west)!0.66!(%s.south west)$) ++(5\\unit,0) ++(0,-10\\unit) edge[handle] ++(0,+20\\unit);",
            nodeCode,
            nodeName, nodeName,
            nodeName, nodeName, nodeName, nodeName,
            nodeName, nodeName,
            nodeName, nodeName, nodeName, nodeName,
            nodeName, nodeName)
        return nodeName, equippedNodeCode
    else
        return getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
    end
end
C.DeuxTiroirsUnePorteOpeningRight = DeuxTiroirsUnePorteOpeningRight

-- Equipement 1 tiroir et 2 portes
local UnTiroirDeuxPortes = Equipment:new()
function UnTiroirDeuxPortes:getTikzNode(seenFrom, anchor, at, extraParams)
    if seenFrom == Volume.SIDE.FRONT then
        local nodeName, nodeCode = getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
        local equippedNodeCode =
            string.format("%s\
  \\path ($(%s.north)!0!(%s.south)$) ++(0,-5\\unit) ++(-10\\unit,0) edge[handle] ++(+20\\unit,0);\
  \\path[cut] ($(%s.north west)!.16!(%s.south west)$) -- ($(%s.north east)!.16!(%s.south east)$);\
  \\path[cut] ($(%s.north)!.16!(%s.south)$) -- (%s.south);\
  \\path ($(%s.north)!.58!(%s.south)$) ++(-5\\unit,0) ++(0,-10\\unit) edge[handle] ++(0,+20\\unit);\
  \\path ($(%s.north)!.58!(%s.south)$) ++(+5\\unit,0) ++(0,-10\\unit) edge[handle] ++(0,+20\\unit);",
            nodeCode,
            nodeName, nodeName,
            nodeName, nodeName, nodeName, nodeName,
            nodeName, nodeName, nodeName,
            nodeName, nodeName,
            nodeName, nodeName)
        return nodeName, equippedNodeCode
    else
        return getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
    end
end
C.UnTiroirDeuxPortes = UnTiroirDeuxPortes

-- Equipement 1 tiroir et 1 porte ouvrant à droite
local UnTiroirUnePorteOpeningRight = Equipment:new()
function UnTiroirUnePorteOpeningRight:getTikzNode(seenFrom, anchor, at, extraParams)
    if seenFrom == Volume.SIDE.FRONT then
        local nodeName, nodeCode = getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
        local equippedNodeCode =
            string.format("%s\
  \\path ($(%s.north)!0!(%s.south)$) ++(0,-5\\unit) ++(-10\\unit,0) edge[handle] ++(+20\\unit,0);\
  \\path[cut] ($(%s.north west)!.16!(%s.south west)$) -- ($(%s.north east)!.16!(%s.south east)$);\
  \\path ($(%s.north west)!.58!(%s.south west)$) ++(+5\\unit,0) ++(0,-10\\unit) edge[handle] ++(0,+20\\unit);",
            nodeCode,
            nodeName, nodeName,
            nodeName, nodeName, nodeName, nodeName,
            nodeName, nodeName)
        return nodeName, equippedNodeCode
    else
        return getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
    end
end
C.UnTiroirUnePorteOpeningRight = UnTiroirUnePorteOpeningRight

-- Equipement 2 portes
local TwoDoors = Equipment:new()
function TwoDoors:getTikzNode(seenFrom, anchor, at, extraParams)
    if seenFrom == Volume.SIDE.FRONT then
        local nodeName, nodeCode = getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
        local equippedNodeCode =
            string.format("%s\
  \\path[cut] (%s.north) -- (%s.south);\
  \\path ($(%s.north)!.5!(%s.south)$) ++(-5\\unit,0) ++(0,-10\\unit) edge[handle] ++(0,+20\\unit);\
  \\path ($(%s.north)!.5!(%s.south)$) ++(+5\\unit,0) ++(0,-10\\unit) edge[handle] ++(0,+20\\unit);",
            nodeCode,
            nodeName, nodeName,
            nodeName, nodeName,
            nodeName, nodeName)
        return nodeName, equippedNodeCode
    else
        return getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
    end
end
C.TwoDoors = TwoDoors

-- Equipement 1 porte
local OneDoorOpeningRight = Equipment:new()
function OneDoorOpeningRight:getTikzNode(seenFrom, anchor, at, extraParams)
    if seenFrom == Volume.SIDE.FRONT then
        local nodeName, nodeCode = getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
        local equippedNodeCode =
            string.format("%s\
  \\path ($(%s.north west)!0.5!(%s.south west)$) ++(5\\unit,0) ++(0,-10\\unit) edge[handle] ++(0,+20\\unit);",
            nodeCode,
            nodeName, nodeName)
        return nodeName, equippedNodeCode
    else
        return getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
    end
end
C.OneDoorOpeningRight = OneDoorOpeningRight

--------------------------------------------------------------------------------

-- Equipement Four
local Oven = Equipment:new()
function Oven:getTikzNode(seenFrom, anchor, at, extraParams)
    if seenFrom == Volume.SIDE.FRONT then
        local nodeName, nodeCode = getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
        local equippedNodeCode =
            string.format("%s\
  \\node[anchor=center] at (%s.center) {\\includegraphics[keepaspectratio, width=%f\\unit, height=%f\\unit]{oven}};",
            nodeCode, nodeName, self.width, self.height)
        return nodeName, equippedNodeCode
    else
        return getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
    end
end
C.Oven = Oven

-- Equipement Plaque de cuisson
local Hob = Equipment:new()
function Hob:getTikzNode(seenFrom, anchor, at, extraParams)
    if seenFrom == Volume.SIDE.TOP then
        local rotateStr = ""
        if extraParams then
           if extraParams.faceRotation then
              rotateStr = string.format(", rotate=%s", extraParams.faceRotation)
           end
        end
        local nodeName, nodeCode = getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
        local equippedNodeCode =
            string.format("%s\
  \\setlength{\\tmpLgthA}{%f\\unit}\
  \\setlength{\\tmpLgthB}{%f\\unit}\
  \\node[anchor=center%s] at (%s.center) {\\includegraphics[keepaspectratio, width=.8\\tmpLgthA, height=.8\\tmpLgthB]{hob}};",
            nodeCode,
            self.width, self.height,
            rotateStr, nodeName)
        return nodeName, equippedNodeCode
    else
        return getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
    end
end
C.Hob = Hob

-- Equipement Lavevaiselle
local Dishwasher = Equipment:new()
function Dishwasher:getTikzNode(seenFrom, anchor, at, extraParams)
    if seenFrom == Volume.SIDE.FRONT then
        local nodeName, nodeCode = getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
        local equippedNodeCode =
            string.format("%s\
  \\node[anchor=center] at (%s.center) {\\includegraphics[keepaspectratio, width=%f\\unit, height=%f\\unit]{dishwasher}};",
            nodeCode, nodeName, self.width, self.height)
        return nodeName, equippedNodeCode
    else
        return getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
    end
end
C.Dishwasher = Dishwasher

-- Equipement Evier double
local DoubleSinkWithDrainerOnLeft = Equipment:new()
function DoubleSinkWithDrainerOnLeft:getTikzNode(seenFrom, anchor, at, extraParams)
    if seenFrom == Volume.SIDE.TOP then
        local rotateStr = ""
        if extraParams then
           if extraParams.faceRotation then
              rotateStr = string.format(", rotate=%s", extraParams.faceRotation)
           end
        end
        local nodeName, nodeCode = getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
        local equippedNodeCode =
            string.format("%s\
  \\node[anchor=east%s] at (%s.east) {\\includegraphics[width=116\\unit, height=50\\unit]{kitchen-sink_doubleWithDrainerLeft}};",
                nodeCode, rotateStr, nodeName)
        return nodeName, equippedNodeCode
    else
        return getmetatable(self):getTikzNode(seenFrom, anchor, at, extraParams)
    end
end
C.DoubleSinkWithDrainerOnLeft = DoubleSinkWithDrainerOnLeft

--------------------------------------------------------------------------------

if _REQUIREDNAME == nil then
    Equipment_LM = C
else
    _G[_REQUIREDNAME] = C
end

-- Import Section:
-- declare everything this package needs from outside

-- no more external access after this point
-- setfenv(1, C) -- TODO solve error

--------------------------------------------------------------------------------

--------------------------------------------------------------------------------

return C

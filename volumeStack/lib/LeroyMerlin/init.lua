local moduleSearched, moduleFile = ...
-- print(string.format("\n\n==> Found '%s' and '%s'", moduleSearched, moduleFile))

local pathOfThisModule =
    string.gsub(
        debug.getinfo(1).source,
        "^@(.+).init.lua$", "%1"
    )
local oldPackagePath = package.path
package.path = package.path .. ";" .. pathOfThisModule .. "/?.lua"

require "Plinthe"
require "Caissons"
require "Equipments"

if not Volume.defaultVolume then
    Volume.setDefaultVolume(ColorVolume:new{ color = "white" })
end

package.path = oldPackagePath

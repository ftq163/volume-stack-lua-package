require "VolumeAtom"

--------------------------------------------------------------------------------

local C = {}

-- CAISSONS BAS

local depths = { S = 35, M = 58 }
local widths = { XXS = 15, XS = 30, Sa = 40, Sb = 45, M = 60, L = 80, XL = 90, XXL = 120 }

C["B"] = {}

for dId, dVal in pairs(depths) do

    C["B"][dId] = {}

    for wId, wVal in pairs(widths) do

        local D = VolumeAtom:new{
            width = wVal, height = 77.8, depth = dVal,
        }

        C["B"][dId][wId] = D
        _G[string.format("Caisson_LM_Bas_D-%s_W-%s", dId, wId)] = D

    end
end

-- CAISSONS COLONNES

local depths = { S = 35, M = 58 }
local heights = { S = 138, M = 215 }
local widths = { S = 15, M = 45, L = 60 }

C["C"] = {}

for dId, dVal in pairs(depths) do

    C["C"][dId] = {}

    for hId, hVal in pairs(heights) do

        C["C"][dId][hId] = {}
    
        for wId, wVal in pairs(widths) do

            local D = VolumeAtom:new{
                width = wVal, height = hVal, depth = dVal,
            }

            C["C"][dId][hId][wId] = D
            _G[string.format("Caisson_LM_Colonne_D-%s_H-%s_W-%s", dId, hId, wId)] = D

        end
    end
end

-- CAISSONS HAUTS

local depths = { M = 35, L = 58 }
local heights = { XXS = 26, XS = 39, S = 48, M = 77, L = 103 }
local widths = { XS = 30, Sa = 40, Sb = 45, M = 60, Mb = 67, L = 80, XL = 90, XXL = 103 }

C["H"] = {}

for dId, dVal in pairs(depths) do

    C["H"][dId] = {}

    for hId, hVal in pairs(heights) do

        C["H"][dId][hId] = {}
    
        for wId, wVal in pairs(widths) do

            local D = VolumeAtom:new{
                width = wVal, height = hVal, depth = dVal,
            }

            C["H"][dId][hId][wId] = D
            _G[string.format("Caisson_LM_Haut_D-%s_H-%s_W-%s", dId, hId, wId)] = D

        end
    end
end

--------------------------------------------------------------------------------

if _REQUIREDNAME == nil then
    Caisson_LM = C
else
    _G[_REQUIREDNAME] = C
end

-- Import Section:
-- declare everything this package needs from outside

-- no more external access after this point
-- setfenv(1, C) -- TODO solve error

--------------------------------------------------------------------------------

--------------------------------------------------------------------------------

return C

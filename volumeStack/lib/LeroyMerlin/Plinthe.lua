require "VolumeAtom"

--------------------------------------------------------------------------------

local C = VolumeAtom:new{
    width = 240, height = 10, depth = 1.3,
}

--------------------------------------------------------------------------------

if _REQUIREDNAME == nil then
   Plinthe = C
else
   _G[_REQUIREDNAME] = C
end

-- Import Section:
-- declare everything this package needs from outside

-- no more external access after this point
-- setfenv(1, C) -- TODO solve error

--------------------------------------------------------------------------------

--------------------------------------------------------------------------------

return C

require "Class"

--------------------------------------------------------------------------------

local Sides = { TOP = "top", BOTTOM = "bottom", LEFT = "left", RIGHT = "right" }

------------------------------------------------------------------------------

local C = Class:new{
   width = 0, height = 0,
   description = "No description"
}

--------------------------------------------------------------------------------

if _REQUIREDNAME == nil then
   Face = C
else
   _G[_REQUIREDNAME] = C
end

-- Import Section:
-- declare everything this package needs from outside
local Utilities = Utilities

-- no more external access after this point
-- setfenv(1, C) -- TODO solve error

--------------------------------------------------------------------------------

C.SIDE = Sides

function C:setParentVolume(volume)
   self.volume = volume
   return self
end

function C:setName(name)
   self.name = name
   return self
end

function C:describe()
   return string.format("This is face '%s' of volume '%s' (width=%.1f, height=%.1f, description='%s')", self.name, self.volume.name, self.width, self.height, self.description)
end

-- Produce Tikz node for this face
function C:getTikzNode(anchor, at, extraParams)
   local params = Utilities.cloneTable(extraParams)
   --[[
   print(string.format(
      "Calling getTikzNode for %s with params: %s!",
      self.name, Utilities.table2str(params))
   )
   --]]
   local nodeStyle = "face"
   local rotateStr = ""
   local extraNodeParams = ""
   local nodeText = self.volume.name

   if params then
      if params.nodeStyle then
         nodeStyle = params.nodeStyle .. ", "
      end
      if params.faceRotation then
         rotateStr = string.format("rotate=%s, ", params.faceRotation)
      end
      if params.extraNodeParms then
         extraNodeParams = params.extraNodeParms
      end
      if params.nodeText then
         nodeText = params.nodeText
      end
   end

   local nodeCode = string.format(
      "\\node[%s%sanchor=%s, minimum width=%f\\unit, minimum height=%f\\unit%s] (%s) at %s {%s};",
      nodeStyle, rotateStr,
      anchor, self.width, self.height,
      extraNodeParams,
      self.name, at, nodeText
   )
   return self.name, nodeCode
end

--------------------------------------------------------------------------------

return C

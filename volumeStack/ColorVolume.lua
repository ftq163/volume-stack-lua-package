require "VolumeAtom"
require "ColorFace"

--------------------------------------------------------------------------------

local C = VolumeAtom:new{
    color = "white"
}

--------------------------------------------------------------------------------

if _REQUIREDNAME == nil then
   ColorVolume = C
else
   _G[_REQUIREDNAME] = C
end

-- Import Section:
-- declare everything this package needs from outside
local ColorFace = ColorFace

-- no more external access after this point
-- setfenv(1, C) -- TODO solve error

--------------------------------------------------------------------------------

function C:getFace(side)
    if not self.faces[side]
    then
        self:setFace(side, ColorFace:new{ color = self.color })
    end
    return self.faces[side]
end

--------------------------------------------------------------------------------

return C

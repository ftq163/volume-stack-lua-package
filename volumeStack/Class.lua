local C = {}   -- package

if _REQUIREDNAME == nil then
   Class = C
else
   _G[_REQUIREDNAME] = C
end

-- Import Section:
-- declare everything this package needs from outside
-- local sqrt = math.sqrt
-- local io = io

-- no more external access after this point
-- setfenv(1, C) -- TODO solve error

--------------------------------------------------------------------------------

function C:new(o)
   o = o or {}   -- create object if user does not provide one
   setmetatable(o, self)
   self.__index = self
   o:init()
   return o
end

function C:init()
   -- print(string.format("In Class.init() where self = %s", self))
end

--------------------------------------------------------------------------------

function C:clone(deep)
   local o = {}
   for k, v in pairs(self) do
      o[k] = v
   end
   if deep then
      print("Probably something to be done here ... but where to stop cloning?")
   else
      setmetatable(o, getmetatable(self))
   end
   -- getmetatable(o).__index = getmetatable(o)
   o:init()
   return o
end

--------------------------------------------------------------------------------

C.nbFreshNamesGenerated = 0
function C:getFreshName(prefix)
   self.nbFreshNamesGenerated = self.nbFreshNamesGenerated + 1
   return string.format("_%s-%i", prefix, self.nbFreshNamesGenerated)
end

return C

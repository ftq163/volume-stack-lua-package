require "Face"

--------------------------------------------------------------------------------

local C = Face:new{
   imagePath = ""
}

--------------------------------------------------------------------------------

if _REQUIREDNAME == nil then
   ImageFace = C
else
   _G[_REQUIREDNAME] = C
end

-- Import Section:
-- declare everything this package needs from outside
local Utilities = Utilities
local Face = Face

-- no more external access after this point
-- setfenv(1, C) -- TODO solve error

--------------------------------------------------------------------------------

-- Produce Tikz node for this face
function C:getTikzNode(anchor, at, extraParams)
   local params = Utilities.cloneTable(extraParams)
   if not params then params = {} end
   params.nodeStyle = "image face"
   params.nodeText = string.format(
      "\\includegraphics[width=%i\\unit, height=%i\\unit]{%s}",
      self.width, self.height, self.imagePath
   )
   -- return getmetatable(self).getTikzNode(self, anchor, at, params)
   return Face.getTikzNode(self, anchor, at, params)
end

--------------------------------------------------------------------------------

return C

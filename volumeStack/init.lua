local moduleSearched, moduleFile = ...
-- print(string.format("\n\n==> Found '%s' and '%s'", moduleSearched, moduleFile))

local pathOfThisModule =
    string.gsub(
        debug.getinfo(1).source,
        "^@(.+).init.lua$", "%1"
    )
local oldPackagePath = package.path
package.path = package.path .. ";" .. pathOfThisModule .. "/?.lua"

require "Utilities"
require "Class"
require "Face"
require "TransparentFace"
require "ColorFace"
require "ImageFace"
require "Volume"
require "VolumeAtom"
require "VolumeContainer"
require "TransparentVolume"
require "ColorVolume"
require "Stack"
require "VerticalStack"
require "HorizontalStack"
require "Shelf"

-- LuaTeX has troubles remembering it has already loaded the above modules ...
-- ... so using 'package.path' instead of 'oldPackagePath'
package.path = package.path .. ";" .. pathOfThisModule .. "/lib/?/init.lua"

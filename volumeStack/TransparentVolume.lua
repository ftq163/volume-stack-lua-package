require "VolumeAtom"
require "TransparentFace"

--------------------------------------------------------------------------------

local C = VolumeAtom:new{
    description = "Transparent volume"
}

--------------------------------------------------------------------------------

if _REQUIREDNAME == nil then
   TransparentVolume = C
else
   _G[_REQUIREDNAME] = C
end

-- Import Section:
-- declare everything this package needs from outside
local TransparentFace = TransparentFace

-- no more external access after this point
-- setfenv(1, C) -- TODO solve error

--------------------------------------------------------------------------------

function C:getFace(side)
    if not self.faces[side]
    then
        self:setFace(side, TransparentFace:new())
    end
    return self.faces[side]
end

--------------------------------------------------------------------------------

return C

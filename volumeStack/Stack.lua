require "VolumeContainer"
require "Face"

------------------------------------------------------------------------------

local Orientations = { HORIZONTAL = "horizontal", VERTICAL = "vertical" }
local Directions = { TOP = "top", BOTTOM = "bottom", LEFT = "left", RIGHT = "right", FORWARD = "front", BACKWARD = "back" }
local Alignments = {
   FRONT = "front", BACK = "back",
   LEFT = "left",   RIGHT = "right",
   TOP = "top",     BOTTOM = "bottom"
}

------------------------------------------------------------------------------

local C = VolumeContainer:new{
   description = "Volume Stack",
   orientation = Orientations.VERTICAL,
   direction = Directions.TOP,
   alignments = { [Alignments.FRONT] = true, [Alignments.BOTTOM] = true },
   nbVolInStack = 0
}

function C:init()
   if self.name == getmetatable(self).name then
      self.name = Class:getFreshName("Stack")
   end
   Volume.init(self)
   self.content = {}
end

function C:describe(params)
   local prefix = ""
   if params and params.identation then
      prefix = string.rep("  ", params.identation) .. "* "
   end
   if not params then params = {} end
   local indent = params.identation or 0

   local alignmentsTab = {}
   for a, b in pairs(self.alignments) do
      if b then table.insert(alignmentsTab, a) end
   end
   local alignments = table.concat(alignmentsTab, ', ')

   local nextLevelIndent = indent + 1
   local contentStr = ""
   for i, c in ipairs(self.content) do
      params.identation = nextLevelIndent
      contentStr = contentStr .. "\n" .. c:describe(params)
   end

   return string.format(
      "%sThis is stack '%s' (width=%f, height=%f, depth=%f) with <%s> direction, alignments <%s> and content:%s",
      prefix,
      self.name, self.width, self.height, self.depth,
      self.direction, alignments, contentStr
   )
end

--------------------------------------------------------------------------------

if _REQUIREDNAME == nil then
   Stack = C
else
   _G[_REQUIREDNAME] = C
end

-- Import Section:
-- declare everything this package needs from outside
local Utilities = Utilities
local Volume = Volume

-- no more external access after this point
-- setfenv(1, C) -- TODO solve error

--------------------------------------------------------------------------------

C.ORIENTATION = Orientations
C.DIRECTION = Directions
C.ALIGNMENT = Alignments

-- Update dimensions of the stack to allow it to contain 'volume'
function C:updateDimensionWith(volume)
   local origWidth = self.width
   local origHeight = self.height
   local origDepth = self.depth

   self.width = math.max(self.width, volume.width)
   self.height = math.max(self.height, volume.height)
   self.depth = math.max(self.depth, volume.depth)

   if self.direction == Directions.TOP or self.direction == Directions.BOTTOM then
      self.height = origHeight + volume.height
   elseif self.direction == Directions.LEFT or self.direction == Directions.RIGHT then
      self.width = origWidth + volume.width
   elseif self.direction == Directions.FORWARD or self.direction == Directions.BACKWARD then
      self.depth = origDepth + volume.depth
   end

   return self
end

-- Add 'volume' to the stack
function C:stack(volume)
   -- self.nbVolInStack = self.nbVolInStack + 1
   -- self.content[self.nbVolInStack] = volume
   table.insert(self.content, volume)
   self.nbVolInStack = #self.content
   self:updateDimensionWith(volume)
   return self
end

-- Recompute dimension of the stack considering its content. Can be usefull when
-- changing direction of the stack.
function C:recomputeDimensions(recursive)
   self.width = 0
   self.height = 0
   self.depth = 0
   for _, volume in ipairs(self.content) do
      if recursive then volume:recomputeDimensions(recursive) end
      self:updateDimensionWith(volume)
   end
   return self
end

-- Set the direction of this stack
function C:setDirection(direction)
   if
      direction == Directions.TOP or direction == Directions.BOTTOM
   then
      self.orientation = Orientations.VERTICAL
   elseif
      direction == Directions.LEFT or Directions.RIGHT
      or direction == Directions.FORWARD or Directions.BACKWARD
   then
      self.orientation = Orientations.HORIZONTAL
   end
   self.direction = direction
   return self
end

-- Set the direction of this stack and the stacks it contains
function C:setDirections(directions)
   if directions then
      if not directions.front then directions.front = 1 end
      local thisStackDirection = directions[directions.front]
      -- local thisStackDirection = table.remove(directions, 1)
      if thisStackDirection then
         self:setDirection(thisStackDirection)
      end
      local directionsFront = directions.front
      for _, v in ipairs(self.content) do
         directions.front = directionsFront + 1
         v:setDirections(directions)
      end
   end
   return self
end

-- Set the alignments of this stack
function C:setAlignments(alignedSides)
   self.alignments = {}
   for _, s in ipairs(alignedSides) do
      self.alignments[s] = true
   end
   for _, v in ipairs(self.content) do
      v:setAlignments(alignedSides)
   end
   return self
end

-- Deciding face growing direction
local function getFaceGrowthDirection(self, seenFrom)
   local growthDirection = nil

   -- Towards TOP
   if
      (((seenFrom == Volume.SIDE.FRONT) or (seenFrom == Volume.SIDE.BACK)) and (self.direction == Directions.TOP))
      or (((seenFrom == Volume.SIDE.TOP) or (seenFrom == Volume.SIDE.BOTTOM)) and (self.direction == Directions.BACKWARD))
      or (((seenFrom == Volume.SIDE.LEFT) or (seenFrom == Volume.SIDE.RIGHT)) and (self.direction == Directions.TOP))
   then
      growthDirection = Face.SIDE.TOP
   -- Towards BOTTOM
   elseif
      (((seenFrom == Volume.SIDE.FRONT) or (seenFrom == Volume.SIDE.BACK)) and (self.direction == Directions.BOTTOM))
      or (((seenFrom == Volume.SIDE.TOP) or (seenFrom == Volume.SIDE.BOTTOM)) and (self.direction == Directions.FORWARD))
      or (((seenFrom == Volume.SIDE.LEFT) or (seenFrom == Volume.SIDE.RIGHT)) and (self.direction == Directions.BOTTOM))
   then
      growthDirection = Face.SIDE.BOTTOM
   -- Towards LEFT
   elseif
      (((seenFrom == Volume.SIDE.FRONT) or (seenFrom == Volume.SIDE.TOP)) and (self.direction == Directions.LEFT))
      or (((seenFrom == Volume.SIDE.BACK) or (seenFrom == Volume.SIDE.BOTTOM)) and (self.direction == Directions.RIGHT))
      or ((seenFrom == Volume.SIDE.LEFT) and (self.direction == Directions.BACKWARD))
      or ((seenFrom == Volume.SIDE.RIGHT) and (self.direction == Directions.FORWARD))
   then
      growthDirection = Face.SIDE.LEFT
   -- Towards RIGHT
   elseif
      (((seenFrom == Volume.SIDE.FRONT) or (seenFrom == Volume.SIDE.TOP)) and (self.direction == Directions.RIGHT))
      or (((seenFrom == Volume.SIDE.BACK) or (seenFrom == Volume.SIDE.BOTTOM)) and (self.direction == Directions.LEFT))
      or ((seenFrom == Volume.SIDE.LEFT) and (self.direction == Directions.FORWARD))
      or ((seenFrom == Volume.SIDE.RIGHT) and (self.direction == Directions.BACKWARD))
   then
      growthDirection = Face.SIDE.RIGHT
   end

   return growthDirection
end

-- Deciding face growing direction
local function getFaceAlignedSides(self, seenFrom)
   local alignedSides = {}

   -- TOP aligned
   if
      (((seenFrom == Volume.SIDE.FRONT) or (seenFrom == Volume.SIDE.BACK)) and (self.alignments[Alignments.TOP]))
      or (((seenFrom == Volume.SIDE.TOP) or (seenFrom == Volume.SIDE.BOTTOM)) and (self.alignments[Alignments.BACK]))
      or (((seenFrom == Volume.SIDE.LEFT) or (seenFrom == Volume.SIDE.RIGHT)) and (self.alignments[Alignments.TOP]))
   then
      alignedSides[Face.SIDE.TOP] = true
   end
   -- BOTTOM aligned
   if
      (((seenFrom == Volume.SIDE.FRONT) or (seenFrom == Volume.SIDE.BACK)) and (self.alignments[Alignments.BOTTOM]))
      or (((seenFrom == Volume.SIDE.TOP) or (seenFrom == Volume.SIDE.BOTTOM)) and (self.alignments[Alignments.FRONT]))
      or (((seenFrom == Volume.SIDE.LEFT) or (seenFrom == Volume.SIDE.RIGHT)) and (self.alignments[Alignments.BOTTOM]))
   then
      alignedSides[Face.SIDE.BOTTOM] = true
   end
   -- LEFT aligned
   if
      (((seenFrom == Volume.SIDE.FRONT) or (seenFrom == Volume.SIDE.TOP)) and (self.alignments[Alignments.LEFT]))
      or (((seenFrom == Volume.SIDE.BACK) or (seenFrom == Volume.SIDE.BOTTOM)) and (self.alignments[Alignments.RIGHT]))
      or ((seenFrom == Volume.SIDE.LEFT) and (self.alignments[Alignments.BACK]))
      or ((seenFrom == Volume.SIDE.RIGHT) and (self.alignments[Alignments.FRONT]))
   then
      alignedSides[Face.SIDE.LEFT] = true
   end
   -- RIGHT aligned
   if
      (((seenFrom == Volume.SIDE.FRONT) or (seenFrom == Volume.SIDE.TOP)) and (self.alignments[Alignments.RIGHT]))
      or (((seenFrom == Volume.SIDE.BACK) or (seenFrom == Volume.SIDE.BOTTOM)) and (self.alignments[Alignments.LEFT]))
      or ((seenFrom == Volume.SIDE.LEFT) and (self.alignments[Alignments.FRONT]))
      or ((seenFrom == Volume.SIDE.RIGHT) and (self.alignments[Alignments.BACK]))
   then
      alignedSides[Face.SIDE.RIGHT] = true
   end

   local growthDirection = getFaceGrowthDirection(self, seenFrom)
   if
      (growthDirection == Face.SIDE.TOP) or (growthDirection == Face.SIDE.BOTTOM)
   then
      alignedSides[Face.SIDE.TOP] = false
      alignedSides[Face.SIDE.BOTTOM] = false
   elseif
      (growthDirection == Face.SIDE.LEFT) or (growthDirection == Face.SIDE.RIGHT)
   then
      alignedSides[Face.SIDE.LEFT] = false
      alignedSides[Face.SIDE.RIGHT] = false
   end

   --[[
   local alignedSidesStrs = {}
   for s, b in pairs(alignedSides) do
      table.insert(alignedSidesStrs, string.format("%s:%s", s, b))
   end
   print(string.format(
      "Seen from %s, %s is aligned on: %s!",
      seenFrom, self.name, table.concat(alignedSidesStrs, ', ')
   ))
   --]]

   return alignedSides
end

-- Compute anchoring of volumes in this stack
local function getNextAnchorAndPosition(self, seenFrom)
   local growthDirection = getFaceGrowthDirection(self, seenFrom)
   local alignedSides = getFaceAlignedSides(self, seenFrom)

   -- print(string.format("Seen from %s, %s grows towards %s and is aligned on %s!", seenFrom, self.name, growthDirection, alignedSide))

   local nextAnchor = nil
   local nextAtNodeCoord = nil

   if growthDirection == Face.SIDE.TOP then
      nextAnchor = "south"
      nextAtNodeCoord = "north"
   elseif growthDirection == Face.SIDE.BOTTOM then
      nextAnchor = "north"
      nextAtNodeCoord = "south"
   elseif growthDirection == Face.SIDE.LEFT then
      nextAnchor = "east"
      nextAtNodeCoord = "west"
   elseif growthDirection == Face.SIDE.RIGHT then
      nextAnchor = "west"
      nextAtNodeCoord = "east"
   end

   local insertAlign = function(coord, inFront)
      if nextAnchor then
         if inFront then
            nextAnchor = string.format("%s %s", coord, nextAnchor)
            nextAtNodeCoord = string.format("%s %s", coord, nextAtNodeCoord)
         else
            nextAnchor = string.format("%s %s", nextAnchor, coord)
            nextAtNodeCoord = string.format("%s %s", nextAtNodeCoord, coord)
         end
      else
         nextAnchor = coord
         nextAtNodeCoord = coord
      end
   end

   if alignedSides[Face.SIDE.TOP] then
      insertAlign("north", true)
   end
   if alignedSides[Face.SIDE.BOTTOM] then
      insertAlign("south", true)
   end
   if alignedSides[Face.SIDE.LEFT] then
      insertAlign("west", false)
   end
   if alignedSides[Face.SIDE.RIGHT] then
      insertAlign("east", false)
   end

   return (nextAnchor or "center"), (nextAtNodeCoord or "center")
end

-- Produce Tikz node for this stack
function C:getTikzNode(seenFrom, anchor, at, extraParams)
   local params = Utilities.cloneTable(extraParams)
   --[[
   print(string.format(
      "Calling getTikzNode for %s with params: %s!",
      self.name, Utilities.table2str(params))
   )
   --]]
   local dfltStyleStr = "stack face"
   local rotateStr = ""

   if params then
      if (seenFrom == Volume.SIDE.TOP) and params.rotationAroundHeightAxis then
         rotateStr = string.format("rotate=%s, ", params.rotationAroundHeightAxis)
         params.faceRotation = params.rotationAroundHeightAxis
      end
   end

   local stackNodeName = string.format("%s_%s", self.name, seenFrom)
   local stackFace = self:getFace(seenFrom)
   local stackCode = string.format(
      "\\node[%s, %sanchor=%s, minimum width=%f\\unit, minimum height=%f\\unit] (%s) at %s {};",
      dfltStyleStr, rotateStr,
      anchor, stackFace.width, stackFace.height,
      stackNodeName, at
   )

   -- print(self.content)
   local nextAnchor, stackNextAtNodeCoord = getNextAnchorAndPosition(self, seenFrom)
   local nextAt = string.format("(%s.%s)", stackNodeName, nextAnchor)
   for i, v in ipairs(self.content) do
      -- print(string.format("Volume '%s'", v.name))
      local nodeName, nodeCode = v:getTikzNode(seenFrom, nextAnchor, nextAt, params)
      -- print(string.format(
      --          "Volume '%s' produced node '%s' whose code is '%s'",
      --          v.name, nodeName, nodeCode
      -- ))
      stackCode = string.format("%s\n%s", stackCode, nodeCode)
      nextAt = string.format("(%s.%s)", nodeName, stackNextAtNodeCoord)
   end

   return stackNodeName, stackCode
end

--------------------------------------------------------------------------------

return C

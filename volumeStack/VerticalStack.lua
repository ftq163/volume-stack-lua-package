require "Stack"

local C = Stack:new{
   orientation = Stack.ORIENTATION.VERTICAL,
   description = "Vertical Stack"
}

--------------------------------------------------------------------------------

if _REQUIREDNAME == nil then
   VerticalStack = C
else
   _G[_REQUIREDNAME] = C
end

-- Import Section:
-- declare everything this package needs from outside
local Stack = Stack

-- no more external access after this point
-- setfenv(1, C) -- TODO solve error

--------------------------------------------------------------------------------

--------------------------------------------------------------------------------

return C

local P = {}   -- package

if _REQUIREDNAME == nil then
   Utilities = P
else
   _G[_REQUIREDNAME] = P
end

-- Import Section:
-- declare everything this package needs from outside
-- local sqrt = math.sqrt
-- local io = io

-- no more external access after this point
-- setfenv(1, C) -- TODO solve error

--------------------------------------------------------------------------------

function P.cloneTable(table)
    if not table then
        return table
    else
        local clone = {}
        for k, v in pairs(table) do
           clone[k] = v
        end
        return clone
    end
end

function P.table2str(t)
   local resTable = {}
   if t then
      for k, v in pairs(t) do
        table.insert(resTable, string.format("%s -> %s", k, v))
      end
   end
   return table.concat(resTable, ', ')
end

--------------------------------------------------------------------------------

return P
require "Face"

--------------------------------------------------------------------------------

local C = Face:new{
   color = "white"
}

--------------------------------------------------------------------------------

if _REQUIREDNAME == nil then
   ColorFace = C
else
   _G[_REQUIREDNAME] = C
end

-- Import Section:
-- declare everything this package needs from outside
local Utilities = Utilities
local Face = Face

-- no more external access after this point
-- setfenv(1, C) -- TODO solve error

--------------------------------------------------------------------------------

-- Produce Tikz node for this face
function C:getTikzNode(anchor, at, extraParams)
   local params = Utilities.cloneTable(extraParams)
   -- print(string.format("Calling getTikzNode for %s!", self.name))
   if not params then params = {} end
   params.nodeStyle = "color face"
   if not params.extraNodeParms then params.extraNodeParms = "" end
   params.extraNodeParms = string.format("%s, fill=%s", params.extraNodeParms, self.color)
   params.nodeText = ""
   -- return getmetatable(self).getTikzNode(self, anchor, at, params)
   return Face.getTikzNode(self, anchor, at, params)
end

--------------------------------------------------------------------------------

return C

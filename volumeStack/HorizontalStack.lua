require "Stack"

local C = Stack:new{
   orientation = Stack.ORIENTATION.HORIZONTAL,
   description = "Horizontal stack"
}

--------------------------------------------------------------------------------

if _REQUIREDNAME == nil then
   HorizontalStack = C
else
   _G[_REQUIREDNAME] = C
end

-- Import Section:
-- declare everything this package needs from outside
local Stack = Stack

-- no more external access after this point
-- setfenv(1, C) -- TODO solve error

--------------------------------------------------------------------------------

--------------------------------------------------------------------------------

return C

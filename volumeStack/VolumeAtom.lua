require "Volume"

--------------------------------------------------------------------------------

local C = Volume:new{
   description = "Atomic volume."
}

--------------------------------------------------------------------------------

if _REQUIREDNAME == nil then
   VolumeAtom = C
else
   _G[_REQUIREDNAME] = C
end

-- Import Section:
-- declare everything this package needs from outside
local Utilities = Utilities

-- no more external access after this point
-- setfenv(1, C) -- TODO solve error

--------------------------------------------------------------------------------

-- Get the face of the volume
-- Return a default one if none have been defined
function C:getFace(side)
   if self.faces[side]
   then
      return self.faces[side]
   elseif Volume.defaultVolume and (self ~= Volume.defaultVolume)
   then
      local dfltFace = Volume.defaultVolume:getFace(side)
      return self:adaptFaceToVolume(side, dfltFace:clone(false))
   else
      print(string.format("%% Face '%s' of volume '%s' does not exists!", side, self.name))
      return self:adaptFaceToVolume(side, Face:new())
   end
end

-- Produce Tikz node for this volume
function C:getTikzNode(seenFrom, anchor, at, extraParams)
   local params = Utilities.cloneTable(extraParams)
   local rotateAngle = nil
   local displayWidth = false

   if params then
      if params.faceRotation then
         rotateAngle = params.faceRotation
      end
      if params.displayWidth then
         displayWidth = true
      end
   end

   local nodeName, nodeCode = Volume.getTikzNode(self, seenFrom, anchor, at, params)
   if displayWidth and self.width > 0 then
      local dispAngle = 90
      if rotateAngle then
         dispAngle = dispAngle + rotateAngle
      end
      nodeCode = string.format(
         "%s\n\\displayDistance{%i}{.5cm}{%s.south west}{%s.south east}",
         nodeCode, dispAngle, nodeName, nodeName
      )
   end
   return nodeName, nodeCode
end

--------------------------------------------------------------------------------

return C

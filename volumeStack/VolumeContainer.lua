require "Volume"

--------------------------------------------------------------------------------

local C = Volume:new{
   description = "Volumes container"
}

--------------------------------------------------------------------------------

if _REQUIREDNAME == nil then
   VolumeContainer = C
else
   _G[_REQUIREDNAME] = C
end

-- Import Section:
-- declare everything this package needs from outside

-- no more external access after this point
-- setfenv(1, C) -- TODO solve error

--------------------------------------------------------------------------------

return C

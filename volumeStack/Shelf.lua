-- API class for Stack of Stacks

require "Stack"

local C = Stack:new{}

function C:init()
   if self.name == getmetatable(self).name then
      self.name = Class:getFreshName("Shelf")
   end
   Stack.init(self)
end

--------------------------------------------------------------------------------

if _REQUIREDNAME == nil then
   Shelf = C
else
   _G[_REQUIREDNAME] = C
end

-- Import Section:
-- declare everything this package needs from outside
local Class = Class

-- no more external access after this point
-- setfenv(1, C) -- TODO solve error

--------------------------------------------------------------------------------

local function addNewInternalStack(self)
   self.currentStack = Stack:new()
   self.savedDirections.front = self.savedDirectionsFront + 1
   self.currentStack:setDirections(self.savedDirections)
   self.currentStack:setAlignments(self.alignedSides)
   Stack.stack(self, self.currentStack)
   return self
end

--------------------------------------------------------------------------------

-- Set the direction of this stack and the stacks it contains
function C:setDirections(directions)
   if directions then
      if not directions.front then directions.front = 1 end
      self.savedDirections = directions
      self.savedDirectionsFront = directions.front
      Stack.setDirections(self, directions)
   end
   return self
end

-- Set the alignments of this stack
function C:setAlignments(alignedSides)
   self.alignedSides = alignedSides
   Stack.setAlignments(self, alignedSides)
   return self
end

--------------------------------------------------------------------------------

function C:stack(volume)
   if not self.currentStack then
      addNewInternalStack(self)
   end
   self.currentStack:stack(volume)
   self:recomputeDimensions()
   -- self:updateDimensionWith(self.currentStack)
   return self
end

function C:nextStack()
   self.currentStack = nil
   return self
end

--------------------------------------------------------------------------------

return C

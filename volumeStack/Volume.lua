require "Class"
require "Face"

--------------------------------------------------------------------------------

local Sides = {
   FRONT = "front",
   BACK = "back",
   LEFT = "left",
   RIGHT = "right",
   TOP = "top",
   BOTTOM = "bottom"
}

--------------------------------------------------------------------------------

local C = Class:new{
   width = 0, height = 0, depth = 0,
   description = "No description"
}

function C:init()
   if self.name == getmetatable(self).name then
      self.name = Class:getFreshName("Volume")
   end
   Class.init(self)
   self.faces = {}
end

--------------------------------------------------------------------------------

if _REQUIREDNAME == nil then
   Volume = C
else
   _G[_REQUIREDNAME] = C
end

-- Import Section:
-- declare everything this package needs from outside
local Class = Class

-- no more external access after this point
-- setfenv(1, C) -- TODO solve error

--------------------------------------------------------------------------------

C.SIDE = Sides

-- Describe the volume
function C:describe(params)
   local prefix = ""
   if params and params.identation
   then
      prefix = string.rep("  ", params.identation) .. "* "
   end
   return string.format(
      "%sThis is volume '%s' (width=%f, height=%f, depth=%f, description='%s')",
      prefix,
      self.name, self.width, self.height, self.depth,
      self.description
   )
end

-- Define the default volume for queries of undefined values for other volumes
function C.setDefaultVolume(volume)
   C.defaultVolume = volume
end

-- Define a face of the volume
function C:adaptFaceToVolume(side, face)
   if not (face.width > 0) then
      if (side == Sides.FRONT) or (side == Sides.BACK)
         or (side == Sides.TOP) or (side == Sides.BOTTOM)
      then
         face.width = self.width
      elseif (side == Sides.LEFT) or (side == Sides.RIGHT) then
         face.width = self.depth
      end
   end
   if not (face.height > 0) then
      if (side == Sides.FRONT) or (side == Sides.BACK)
         or (side == Sides.LEFT) or (side == Sides.RIGHT)
      then
         face.height = self.height
      elseif (side == Sides.TOP) or (side == Sides.BOTTOM) then
         face.height = self.depth
      end
   end
   face:setParentVolume(self)
   face:setName(string.format("%s_%s", self.name, side))
   return face
end

-- Define a face of the volume
function C:setFace(side, face)
   self.faces[side] = self:adaptFaceToVolume(side, face)
   return self
end

-- Get the face of the volume
-- Return a default one if none have been defined
function C:getFace(side)
   if self.faces[side]
   then
      return self.faces[side]
   else
      print(string.format("%% Face '%s' of volume '%s' does not exists!", side, self.name))
      return self:adaptFaceToVolume(side, Face:new())
   end
end

-- Produce Tikz node for this volume
function C:getTikzNode(seenFrom, anchor, at, params)
   if params then
      if (seenFrom == Volume.SIDE.TOP) and params.rotationAroundHeightAxis then
         params.faceRotation = params.rotationAroundHeightAxis
      end
   end

   local face = self:getFace(seenFrom)
   return face:getTikzNode(anchor, at, params)
end

--------------------------------------------------------------------------------

-- Stop the stack function when reaching atomic volumes
function C:setDirections(directions)
   return self
end

-- Stop the stack function when reaching atomic volumes
function C:setAlignments(alignments)
   return self
end

-- Do not recompute dimensions for atomic volumes
function C:recomputeDimensions(recursive)
   return self
end

--------------------------------------------------------------------------------

return C
